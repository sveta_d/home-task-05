Розробити компонент "Smart Table" - таблиця, колонки котрої представляють дані з масиву об'єктів.
Заголовки колонок - це назви полів в об'єкті. При натисканні на заголовок таблиця має бути відсортована по даному полю,
при повторному натисканню на цей же заголовок - відсортована в зворотньому порядку.
Для сортування має бути передбачено програмний інтерфейс (метод **sort**).
Таблиця вбудовується в елемент, який задано параметром **parentElement**.