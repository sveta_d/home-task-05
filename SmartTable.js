/**
 * Component with ability to sort column by clicking on corresponding header
 * @param {HTMLElement} parentElement
 * @param {Array.<Object>} array
 * @constructor
 */
function SmartTable(parentElement, array) {
    this.array = array;
    this.table = parentElement.ownerDocument.createElement('table');

    this.thead = this.table.ownerDocument.createElement('thead');
    var tr = this.thead.ownerDocument.createElement('tr');

    this.maxLength = [0, {}];
    var td,
        self = this;

    parentElement.appendChild(this.table);
    this.table.appendChild(this.thead);
    this.thead.appendChild(tr);

    for(var i = 0; i < this.array.length; i++){
        if(this.maxLength[0] < Object.keys(this.array[i]).length){
            this.maxLength[0] = Object.keys(this.array[i]).length;
            this.maxLength[1] = Object.keys(this.array[i]);
        }
    }

    for(var j=0; j < this.maxLength[0]; j++) {
        td = tr.ownerDocument.createElement('td');
        tr.appendChild(td).textContent = this.maxLength[1][j];
    }

    for(var o = 0; o < this.thead.rows[0].cells.length; o++) {
        this.thead.rows[0].cells[o].onclick = function (){
            self.table.removeChild(self.tbody);
            sortingFunction(this);
        }
    }

    function sortingFunction(element) {
        var td;
        self.tbody = self.table.ownerDocument.createElement('tbody');
        self.table.appendChild(self.tbody);

        for (var i=0; (td = element.parentNode.getElementsByTagName("td").item(i)); i++) {
            if (td.innerText == element.innerText) {
                self.cellIndex = i;
                if (td.sorting == "yes") {
                    self.sortFields.reverse();
                    self.arrow = td.firstChild;
                    self.elementUp = Number(!self.elementUp);
                }else{
                    for (var f = 0; f < self.array.length; f++) {
                        self.sortFields[f] = [];
                        self.sortFields[f][0] = self.array[f][self.maxLength[1][self.cellIndex]];
                        self.sortFields[f][1] = self.array[f];
                    }
                    self.sortFields.sort();
                    td.sorting = "yes";
                    self.arrow = td.insertBefore(document.createElement("span"),td.firstChild);
                    self.elementUp = 0;
                }
                self.arrow.innerText = self.elementUp ? String.fromCharCode(9660) : String.fromCharCode(9650);
            }else{
                if (td.sorting == "yes"){
                    td.sorting = "no";
                    if (td.firstChild){
                        td.removeChild(td.firstChild);
                    }
                }
            }
        }

        for (var k = 0; k <  self.sortFields.length; k++) {
            tr = self.tbody.ownerDocument.createElement('tr');
            for (var j = 0; j < self.maxLength[0]; j++) {
                td = tr.ownerDocument.createElement('td');
                self.sortFields[k][1][self.maxLength[1][j]] ? tr.appendChild(td).textContent =  self.sortFields[k][1][self.maxLength[1][j]] : tr.appendChild(td).textContent = " - ";
            }
            self.tbody.appendChild(tr);
        }
    }
}
/**
 * Sort table by 'column' in ascending order if 'asc' == true, in descending order otherwise
 * @param {string} column
 * @param {boolean} asc
 */

SmartTable.prototype.sort = function(column, asc) {
    var tr, td;
    this.sortFields = [];
    this.cellIndex = null;

    this.tbody = this.table.ownerDocument.createElement('tbody');
    this.table.appendChild(this.tbody);

    for(var c = 0; c < this.maxLength[0]; c++) {
        if(column == this.maxLength[1][c]){
            this.cellIndex = c;
        }
    }

    for(var i = 0; i < this.array.length; i++) {
        this.sortFields[i] = [];
        this.sortFields[i][0] = this.array[i][column];
        this.sortFields[i][1] = this.array[i];
    }
    this.sortFields.sort();

    if(asc == false) {
        this.sortFields.reverse();
    }

    for(var k = 0; k < this.sortFields.length; k++){
        tr = this.tbody.ownerDocument.createElement('tr');
        for(var j=0; j < this.maxLength[0]; j++){
            td  = tr.ownerDocument.createElement('td');
            this.sortFields[k][1][this.maxLength[1][j]] ? tr.appendChild(td).textContent = this.sortFields[k][1][this.maxLength[1][j]] : tr.appendChild(td).textContent = " - ";
        }
        this.tbody.appendChild(tr);
    }

    this.thead.rows[0].cells[this.cellIndex].sorting = 'yes';
    this.elementUp = Number(asc);
    this.arrow =  this.table.rows[0].cells[this.cellIndex].insertBefore(document.createElement("span"), this.table.rows[0].cells[this.cellIndex].firstChild);
    this.arrow.innerText = asc ? String.fromCharCode(9660) : String.fromCharCode(9650);
};




